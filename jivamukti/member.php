<?php


get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class('single-teacher-template'); ?> >
        <div class="page-head">
            <div class="container jv-container">
                <a href="<?php echo get_post_type_archive_link( 'teacher-training' ); ?>" class="redirect-link back">
                    Back to teachers
                </a>
                <hgroup class="title-group">
                    <h3 class="post-type-title brown text-center">Jivamukti Teachers</h3>
                    <h1 class="block-title brown"><?php the_title(); ?></h1>
                </hgroup>
            </div>
        </div>
        <div class="single-teacher-block">
            <div class="container jv-container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="teacher-avatar" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/Cory.jpg);"></div>
                    </div>
                    <div class="col-md-7">
                        <div class="teacher-info">
                            <h3 class="teacher-hours post-type-title brown">300 Hour Certified</h3>
                            <p class="teacher-city">Washington, DC</p>
                            <p class="teacher-country">United States</p>
                            <p class="teacher-site">beherenowyogadc.com</p>
                            <p class="teacher-email">corybryant@gmail.com</p>
                            <p class="teacher-phone">+1 917.921.6939</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="entry-content">
            <div class="container jv-container">
                <h3 class="post-type-title brown">About Cory</h3>
                <?php the_content();?>
            </div>
        </div>



    </div>
    </div>

<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>