<?php
get_header();
?>
<div class="search-template">
    <div class="full-width-img">
       <img src="<?php echo get_template_directory_uri()?>/images/search-image.jpg">
    </div>
    <?php $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
    <div class="wide-image" style="background-image: url('<?php echo get_template_directory_uri()?>/images/search-image.jpg')"></div>
    <div class="container jv-container">
        <?php if ( have_posts() ) : ?>
            <h1 class="block-title">Search Results for "<?php echo get_search_query();?>"</h1>
            <div class="search-results-posts">
                <?php while ( have_posts() ) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <a class="entry-title" href="<?php the_permalink();?>"><?php the_title(); ?></a>
                        <a class="entry-summary" href="<?php the_permalink();?>">
                            <?php the_excerpt(); ?>
                            <span class="read-more-btn">Read more</span>
                        </a>
                    </article>
                <?php endwhile; ?>
                <div class="pagination-links">
                    <?php the_posts_pagination( ); ?>
                </div>
            </div>

        <?php else : ?>
            <h1 class="block-title">Nothing Found</h1>
            <p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>
        <?php endif; ?>

    </div>
</div>
<?php get_footer(); ?>