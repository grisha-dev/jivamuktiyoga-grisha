<?php
/** Template Name: Prerequisites */
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div id="post-<?php the_ID(); ?>" <?php post_class('template-prerequisites'); ?> >
    <section class="page-titles">
        <div class="container jv-container">
        <a href="<?php the_field('teacher_training_page', 'options') ?>" class="redirect-link back">
            Back to jivamukti teacher training
        </a>
        <hgroup class="title-group">
            <h3 class="post-type-title brown text-center">300 Hr Teacher Training</h3>
            <h1 class="block-title brown"><?php the_title(); ?></h1>
        </hgroup>
        </div>
    </section>
    <?php if ( has_post_thumbnail() ) : ?>
        <div class="full-width-img">
            <?php the_post_thumbnail();?>
        </div>
        <?php $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
        <div class="wide-image" style="background-image: url(<?php echo $thumbnail[0]; ?>)"></div>
    <?php else: ?>
        <div class="noimg-block thumbnail"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" class="big-logo"></div>
    <?php endif; ?>
    <?php if (get_the_content()): ?>
    <div class="container jv-container entry-content">
        <?php the_content();?>
    </div>
        <?php endif;?>
    <?php
    $accordion_fields = get_field('accordion_1');
    if ($accordion_fields):
        foreach ($accordion_fields as $accordion_field) :
            if ($accordion_field['title'] != '' && $accordion_field['content'] != '') : ?>
                <section class="prerequisites-accordion">
                    <div class="container jv-container">
                        <div class="accodion-block">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <?php $count = 0; ?>
                                <?php while (have_rows('accordion_1')) : the_row(); ?>
                                    <?php if (get_sub_field('title') && get_sub_field('content')): ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading<?php echo $count; ?>">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle" role="button" data-toggle="collapse"
                                                       href="#collapse<?php echo $count; ?>" aria-expanded="true"
                                                       aria-controls="collapse<?php echo $count; ?>">
                                                        <?php the_sub_field('title'); ?>
                                                        <i class="indicator fa fa-caret-right pull-right"
                                                           aria-hidden="true"></i>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse<?php echo $count; ?>"
                                                 class="panel-collapse collapse"
                                                 role="tabpanel" aria-labelledby="heading<?php echo $count; ?>">
                                                <div class="panel-body">
                                                    <div class="panel-content clearfix">
                                                        <?php the_sub_field('content'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $count++; ?>
                                    <?php endif; ?>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </section>
                <?php break; ?>
            <?php endif;
        endforeach;
    endif; ?>
    <?php $args = array(
        'post_type'			=> 'teacher-training',
        'posts_per_page'	=>  -1,
        'post_status'       => 'publish',
        'meta_query' => array(
            array(
                'key' => 'begin',
                'value'   => date('Ymd'),
                'type'    => 'numeric',
                'compare' => '>',
            ),
        ),
        'tax_query' => array(
            array(
                'taxonomy' => 'teacher_training_categories',
                'field'    => 'slug',
                'terms'    => 'active',
            ),
        ),
        'meta_key'	=> 'begin',
        'orderby' => 'meta_value_num',
        'order' => 'DESC',
    );
    $the_query = new WP_Query( $args ); ?>
    <?php if ( $the_query->have_posts() ) : ?>
    <section class="upcoming-teacher-trainings">
        <div class="container-fluid">
            <div class="row">

                    <h2 class="content-title brown">Upcoming Teacher Trainings</h2>
                    <?php $counter = 0; ?>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <?php if ($counter % 3 == 0) {
                            $class = 'brown-tt';
                        } elseif ($counter % 3 == 1) {
                            $class = 'green-tt';
                        }
                        else {
                            $class = 'pink-tt';
                        } ?>
                        <div class="training-container">
                            <?php if ( has_post_thumbnail() ) :
                                $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
                                <div class="training-item" style="background-image: url(<?php echo $thumbnail[0]; ?>); order: <?php echo ($counter % 2 == 0) ? '0' : '1'?>"></div>
                            <?php else: ?>
                                <div class="noimg-block" style="order: <?php echo ($counter % 2 == 0) ? '0' : '1'?>"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" class="big-logo"></div>
                            <?php endif; ?>

                            <div class="event-info-block <?php echo $class; ?>">
                                <div class="event-info-content">
                                    <p class="event-info-header">Teacher Training</p>
                                    <p class="event-info-country">in <?php the_field('country'); ?></p>
                                    <p class="event-info-date"><?php echo date('F j', strtotime(get_field('begin'))); ?> - <?php the_field('end'); ?></p>
                                    <p class="event-info-hour"><?php the_field('hours'); ?> Hour</p>
                                    <a href="<?php the_permalink(); ?>" class="jv-btn btn-white btn-border">
                                        More info
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php $counter++; ?>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>


        </div>
    </section>
    <?php endif; ?>
    <?php if(get_field('pr_shopify_collection_id')): ?>
    <section class="shopify-products ">
        <div class="container jv-container">
                <h3 class="text-left shop-title">Related Items of Interest</h3>
        </div>
        <div class="container text-center">
            <div class="row">

                <div class="shop-container" data-collection-id="<?php the_field('pr_shopify_collection_id'); ?>" id="product-collection"></div>

            </div>

        </div>
    </section>
    <?php endif;?>
    <section class="have-questions-block container questions-block-border text-center" style="">
        <div class="have-questions-title post-type-title brown">if you have any questions</div>
        <a href="<?php the_field('contact_us', 'option'); ?>" class="jv-btn btn-braun btn-border">Contact us</a>
    </section>

</div>

</div>
<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>