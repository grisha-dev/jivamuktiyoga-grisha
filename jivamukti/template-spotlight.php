<?php
/** Template Name: Spotlight */
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class('template-spotlight'); ?> >
        <hgroup class="template-title-group">
            <h1 class="block-title template-title brown"><?php the_title(); ?></h1>
        </hgroup>
        <?php
        $post_object = get_field('spotlight_top')[0];
        if ($post_object):
            $post = $post_object;
            setup_postdata($post);
            ?>
            <section class="focus-moment-block">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="image-block"
                                 style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'full')[0]; ?>)">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="description-block">
                                <div class="date-moment">
                                    <p class="date-month"><?php echo date('F, Y'); ?></p>
                                </div>
                                <hgroup class="title-group">
                                    <div class="content-title brown text-left"><?php the_title(); ?></div>
                                </hgroup>
                                <?php the_excerpt(); ?>
                                <a href="<?php echo get_the_permalink(); ?>" class="read-more-btn">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php wp_reset_postdata();?>
        <?php endif; ?>
        <section class="fotm-posts">
            <div class="container jv-container">
                <div class="posts-block-top">
                    <h3 class="posts-block-title">Focus Of The Month</h3>
                    <a class="redirect-link more" href="<?php echo get_post_type_archive_link('fotm'); ?>">View all FOTMs</a>
                </div>
                <div class="posts-container">
        <?php
        $counter = 0;
        for ($i = date('Y'); $i > 0; $i--) {
//            echo '$i' . $i . '<br>';
            for ($j = $i . '12'; $j >= $i . '01'; $j-- ) {
                $begin = $j . '01';
                $end = $j . '31';
                $args = array(
                    'post_type' => 'fotm',
                    'posts_per_page' => 1,
                    'post_status' => 'publish',
                    'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key'     => 'post_date',
                            'value'   => array($begin,$end),
                            'type'    => 'numeric',
                            'compare' => 'BETWEEN',
                        ),
                        array(
                            'key'     => 'post_date',
                            'value'   => date('Ymd'),
                            'type'    => 'numeric',
                            'compare' => '<=',
                        ),
                    ),
                    'meta_key' => 'post_date',
                    'orderby' => 'meta_value_num',
                    'order' => 'DESC'
                );
                $the_query = new WP_Query( $args ); ?>
                <?php if ( $the_query->have_posts() ) : ?>
                    <?php         $counter++;?>
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                   <?php get_template_part('template-parts/3-posts-line'); ?>
                                <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>


                <?php endif; ?>
                <?php
//        echo '$j' . $j.'<br>';

        if ($counter == 3) break 2;
            }
        } ?>
                </div>
            </div>
        </section>
        <?php
        $args = array(
            'post_type' => 'community-journals',
            'posts_per_page' => 3,
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key'     => 'post_date',
                    'value'   => date('Ymd'),
                    'type'    => 'numeric',
                    'compare' => '<=',
                ),
            ),
            'meta_key' => 'post_date',
            'orderby' => 'meta_value_num',
            'order' => 'DESC'
        );
        $the_query = new WP_Query( $args ); ?>
        <?php if ( $the_query->have_posts() ) : ?>
            <section class="community-journals-posts">
                <div class="container jv-container">
                    <div class="posts-block-top">
                        <h3 class="posts-block-title">Community Journals</h3>
                        <a class="redirect-link more" href="<?php echo get_post_type_archive_link($args['post_type']); ?>">View all Community Journals</a>
                    </div>
                    <div class="posts-container">
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <?php get_template_part('template-parts/3-posts-line'); ?>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </section>
        <?php endif; ?>
        <?php
        $args = array(
            'post_type' => 'news',
            'posts_per_page' => 3,
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key'     => 'post_date',
                    'value'   => date('Ymd'),
                    'type'    => 'numeric',
                    'compare' => '<=',
                ),
            ),
            'meta_key' => 'post_date',
            'orderby' => 'meta_value_num',
            'order' => 'DESC'
        );
        $the_query = new WP_Query( $args ); ?>
        <?php if ( $the_query->have_posts() ) : ?>
            <section class="news-posts">
                <div class="container jv-container">
                    <div class="posts-block-top">
                        <h3 class="posts-block-title">News</h3>
                        <a class="redirect-link more" href="<?php echo get_post_type_archive_link($args['post_type']); ?>">View all News</a>
                    </div>
                    <div class="posts-container">
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <?php get_template_part('template-parts/3-posts-line'); ?>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </section>
        <?php endif; ?>
    </div>
    <?php
endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>