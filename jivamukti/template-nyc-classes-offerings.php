<?php
/** Template Name: NYC Classes & Offerings */
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div id="post-<?php the_ID(); ?>" <?php post_class('template-nyc-classes-offerings'); ?> >

    <hgroup class="template-title-group">
        <h1 class="block-title template-title brown"><?php the_title(); ?></h1>
    </hgroup>
    <?php if ( has_post_thumbnail() ) : ?>
        <div class="full-width-img">
            <?php the_post_thumbnail();?>
        </div>
        <?php $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
        <div class="wide-image" style="background-image: url(<?php echo $thumbnail[0]; ?>)"></div>
    <?php else: ?>
        <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" class="big-logo"></div>
    <?php endif; ?>
    <section class="jv-schedule-block">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="schedule-title">Class Schedule</h2><!--/.content-title-->
                    <div class="healcode-wrap">
                        <a href="<?php the_field('buy_classes_url'); ?>" target="_blank" class="buy-classes jv-btn-green">Buy classes</a>
                        <healcode-widget data-type="schedules" data-widget-partner="mb" data-widget-id="3a334596fd0" data-widget-version="0.1"></healcode-widget>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    $posts = get_field('classes');
    if( $posts ): ?>
    <section class="jv-classes upcoming-teacher-trainings text-center">
        <div class="container-fluid">
            <div class="row">
                <h2 class="content-title brown">Class Descriptions</h2>
                <?php $counter = 0; ?>
                <?php foreach( $posts as $post):
                    setup_postdata($post);
                        include(locate_template('template-parts/nyc-classes.php',false,false));
                    $counter++; ?>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>
                <a class="jv-btn btn-braun btn-border" href="<?php echo get_post_type_archive_link('classes');?>">View More Class Types</a>
            </div>
        </div>
    </section>
    <?php endif;?>
    <?php
    $post_object = get_field('private_classes_page');
    if( $post_object ):
        $post = $post_object;
        setup_postdata( $post );
        $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
            <section class="private-classes" style="background-image: url(<?php echo $thumbnail[0]; ?>)">
                <div class="private-classes-content">
                    <h3 class="private-classes-title white"><?php the_title();?></h3>
                    <div class="private-classes-desc"><?php the_field('priv_class_description');?></div>
                    <a class="jv-btn btn-border btn-white" href="<?php the_permalink();?>">More info</a>
                </div>
            </section>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
    <?php endif; ?>
    <?php
    global $wpdb;
    $counter = 0;
    $city_field_id = xprofile_get_field_id_from_name('City');
    $term_taxonomy_id = $wpdb->get_var("SELECT term_id FROM " . $wpdb->prefix . "terms WHERE slug = 'massagist'");

//    $user_ids = $wpdb->get_results(  "SELECT object_id FROM " . $wpdb->prefix . "term_relationships WHERE term_taxonomy_id = (SELECT term_id FROM " . $wpdb->prefix . "terms WHERE slug = 'massagist') ORDER BY RAND()"); ?>
<?php     $user_ids = $wpdb->get_results(  "SELECT object_id FROM " . $wpdb->prefix . "term_relationships WHERE term_taxonomy_id = ". $term_taxonomy_id ." ORDER BY RAND()"); ?>

<!--    --><?php //var_dump($user_ids);?>
    <?php if($user_ids): ?>
        <section class="massage-therapy nyc-teachers-block">
            <div class="container jv-container text-center">
                <h2 class="massage-therapy-title brown">Massage Therapy</h2>
                <div class="massage-therapy-desc"><?php the_field('massage_therapy_description');?></div>
            </div>
            <div class="container text-center">
                <div class="massagist-block teachers-block">
                    <?php foreach ($user_ids as $id):?>

                            <?php $avatar_url = bp_core_fetch_avatar(
                                array(
                                    'item_id' => $id->object_id, // id of user for desired avatar
                                    'type' => 'full',
                                    'html' => FALSE     // FALSE = return url, TRUE (default) = return img html
                                )
                            ); ?>
                            <div class="single-massagist single-teacher">
                                <div class="massagist-info teacher-info">

                                    <a href="<?php echo bp_core_get_user_domain($id->object_id); ?>" class="teacher-img" style="background-image: url(<?php echo $avatar_url; ?>)"></a>
                                    <!--/.teacher-img-->
                                    <p class="massagist-name teacher-name"><a href="<?php echo bp_core_get_user_domain($id->object_id); ?>"> <?php echo bp_core_get_core_userdata($id->object_id)->display_name; ?></a></p><!--/.teacher-name-->
                                    <p class="massagist-desc">Massage Therapist</p>
                                </div><!--/.teacher-info-->

                            </div><!--/.single-teacher-->
                            <?php $counter++; ?>
                            <?php  if($counter == 7) break; ?>

                    <?php endforeach; ?>
                </div><!--/.teachers-block-->
                <?php bp_member_hidden_fields(); ?>
                <a href="<?php the_field('massage_therapy_page_url'); ?>" class="jv-btn btn-border btn-braun">More info</a>
            </div><!--/.container-->
        </section><!--/.teacher-training-block-->
    <?php endif;?>
    <?php if (get_field('nyc_jivamukti_cafe_image') || get_field('nyc_jivamukti_cafe_description')):?>
    <section class="nyc-jv-cafe text-center">
        <div class="container jv-container">
            <h2 class="jv-cafe-title white">NYC Jivamukti Cafe</h2>
            <div class="jv-cafe-img" style="background-image: url(<?php the_field('nyc_jivamukti_cafe_image');?>)"></div>
            <div class="jv-cafe-desc"><?php the_field('nyc_jivamukti_cafe_description');?></div>
            <a class="jv-btn btn-border btn-white" target="_blank" href="<?php the_field('nyc_jivamukti_cafe_url');?>">More info</a>
        </div>
    </section>
        <?php endif;?>
<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>
