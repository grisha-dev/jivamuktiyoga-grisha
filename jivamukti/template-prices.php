<?php
/** Template Name: Prices */
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class('template-prices'); ?> >
        <hgroup class="template-title-group">
            <h1 class="block-title template-title brown"><?php the_title(); ?></h1>
        </hgroup>
        <?php if ( has_post_thumbnail() ) : ?>
            <div class="full-width-img">
                <?php the_post_thumbnail();?>
            </div>
            <?php $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
            <div class="wide-image" style="background-image: url(<?php echo $thumbnail[0]; ?>)"></div>
        <?php else: ?>
            <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" class="big-logo"></div>
        <?php endif; ?>
        <section class="prices text-center">

            <div class="container jv-container">
                <div class="row">
                    <?php $table = get_field('drop-ins_and_memberships');
                    for ($i = 0; $i < count($table['body']); $i++) {
                        for ($j = 0; $j < count($table['body'][$i]); $j++) {
                            if (!empty($table['body'][$i][$j]['c'])) { ?>
                                <div class="col-md-6">
                                    <h3>Drop-ins and Memberships</h3>
                                    <div class="table-wrap">
                                        <table class="table price-table">
                                            <?php
                                            foreach ($table['body'] as $tr) {
                                                echo '<tr>';
                                                foreach ($tr as $td) {
                                                    if ($td['c']) {
                                                        echo '<td>';
                                                        if (has_shortcode($td['c'],'sign_up_button'))
                                                            echo do_shortcode($td['c']);
                                                        else
                                                            echo $td['c'];
                                                        echo '</td>';
                                                    }
                                                }
                                                echo '</tr>';
                                            }
                                            ?>
                                        </table>
                                    </div>
                                </div>
                                <?php break 2; ?>
                            <?php }
                        }
                    } ?>
                    <?php $table = get_field('class_packs');
                    for ($i = 0; $i < count($table['body']); $i++) {
                        for ($j = 0; $j < count($table['body'][$i]); $j++) {
                            if (!empty($table['body'][$i][$j]['c'])) { ?>
                                <div class="col-md-6">
                                    <h3>Class Packs</h3>
                                    <div class="table-wrap">
                                        <table class="table price-table">
                                            <?php
                                            foreach ($table['body'] as $tr) {
                                                echo '<tr>';
                                                foreach ($tr as $td) {
                                                    echo '<td>';
                                                    if (has_shortcode($td['c'],'sign_up_button'))
                                                        echo do_shortcode($td['c']);
                                                    else
                                                        echo $td['c'];
                                                    echo '</td>';
                                                }
                                                echo '</tr>';
                                            }
                                            ?>
                                        </table>
                                    </div>
                                </div>

                                <?php break 2; ?>
                            <?php }
                        }
                    } ?>

                </div>
            </div>

    </section>
        <section class="special-promotions">
            <div class="container jv-container">
                <?php $post_objects = get_field('special_promotios');
                if( $post_objects ): ?>
                <h3 class="block-title-21">Special Promotions</h3>
                <div class="pages-block">
                    <?php foreach( $post_objects as $post): ?>
                        <?php setup_postdata($post);
                            $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
                            <div class="single-page-rectangle" style="background-image: url(<?php echo $thumbnail[0]; ?>)">
                                <div class="page-rectangle-content page-rectangle-title">
                                    <?php the_field('promotion_title'); ?>
                                    <?php if(get_field('extra_info')): ?>
                                        <p><?php the_field('extra_info');?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                </div>
                <?php endif; ?>
            </div>
        </section>
        <section class="gift-certificates">
            <div class="container jv-container">
                <h3 class="block-title-21">Gift Certificates</h3>
                <div class="gift-desc"><?php the_field('gift_certificates_description');?></div>
                <div class="row">
                    <?php $table = get_field('gift_certificates_table');
                    for ($i = 0; $i < count($table['body']); $i++) {
                        for ($j = 0; $j < count($table['body'][$i]); $j++) {
                            if (!empty($table['body'][$i][$j]['c'])) { ?>
                                <div class="col-md-6">

                                    <div class="table-wrap">
                                        <table class="table price-table gift">
                                            <?php
                                            foreach ($table['body'] as $tr) {
                                                echo '<tr>';
                                                foreach ($tr as $td) {
                                                    echo '<td>';
                                                    if (has_shortcode($td['c'],'sign_up_button'))
                                                        echo do_shortcode($td['c']);
                                                    else
                                                        echo $td['c'];
                                                    echo '</td>';
                                                }
                                                echo '</tr>';
                                            }
                                            ?>
                                        </table>
                                    </div>
                                </div>

                                <?php break 2; ?>
                            <?php }
                        }
                    } ?>
                    <?php
                    $image = get_field('gift_certificates_image');
                    if( !empty($image) ): ?>
                    <div class="col-md-6">
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </div>
<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>
