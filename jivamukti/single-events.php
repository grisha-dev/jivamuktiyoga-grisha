<?php
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
    <div class="page-titles">
        <div class="container jv-container">
            <a href="<?php echo get_post_type_archive_link('events'); ?>" class="redirect-link back">
                Back to Events
            </a>
            <hgroup class="title-group">
                <h3 class="post-type-title text-center">Jivamukti Event</h3>
                <h1 class="block-title brown"><?php the_title(); ?></h1>
            </hgroup>
        </div>
    </div>
    <div class="single-event-info">
        <div class="container jv-container">
            <div class="row">
                <div class="col-md-6 event-img-wrap">
                    <?php if($image = get_field('image')):
                        $img_medium = $image['sizes'][ 'medium' ];
                        ?>
                        <div class="event-img" style="background-image: url(<?php echo $img_medium; ?>)"></div>
                    <?php else: ?>
                        <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" ></div>
                    <?php endif; ?>
                </div>
                <div class="col-md-6">
                    <div class="event-desc">
                        <p class="event-from">
                            <span class="semi-bold">From: </span><?php the_field('begin'); ?>
                        </p>
                        <p class="event-to">
                            <span class="semi-bold">To: </span><?php the_field('end'); ?>
                        </p>
                        <?php if(get_field('location')): ?>
                            <p class="event-location">
                                <span class="semi-bold">Location: </span><?php echo get_field('location')['address']; ?>
                            </p>
                        <?php endif;?>
                        <?php if(get_field('price')): ?>
                            <p class="event-price">
                                <span class="semi-bold">Price: </span> <?php the_field('price'); ?>
                            </p>
                        <?php endif;?>
                        <?php if(get_field('registration_url')): ?>
                           <a href="<?php the_field('registration_url'); ?>" target="_blank" class="jv-btn-green btn-sign-up">Register</a>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrap">
        <div class="container jv-container">
            <div class="entry-content">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
    <?php if(get_field('teachers')): ?>
    <div class="faculty">
        <?php
        //        var_dump(get_field('teachers'));
        $teaches = get_field('teachers'); ?>
        <div class="nyc-teachers-block">
            <div class="container jv-container">
                <h3 class="block-title-21">Faculty</h3>
            </div>
            <div class="container text-center">
                <div class="teachers-block faculty-teachers">
                    <?php foreach ($teaches as $teacher): ?>
                            <?php $avatar_url = bp_core_fetch_avatar(
                                array(
                                    'item_id' => $teacher['ID'], // id of user for desired avatar
                                    'type' => 'full',
                                    'html' => FALSE     // FALSE = return url, TRUE (default) = return img html
                                )
                            ); ?>
                            <div class="single-teacher">
                                <div class="teacher-info">
                                    <a href="<?php echo bp_core_get_user_domain($teacher['ID']); ?>" class="teacher-img" style="background-image: url(<?php echo $avatar_url; ?>)"></a>
                                    <!--/.teacher-img-->
                                    <p class="teacher-name"><a href="<?php echo bp_core_get_user_domain($teacher['ID']); ?>"> <?php echo bp_core_get_core_userdata($teacher['ID'])->display_name; ?></a>
                                    </p><!--/.teacher-name-->
                                </div><!--/.teacher-info-->
                                <?php
                                if (xprofile_get_field_data('Certificate', $teacher['ID'])): ?>
                                    <p class="teacher-hour"><?php echo xprofile_get_field_data('Certificate', $teacher['ID']); ?> Certified</p><!--/.teacher-hour-->
                                <?php endif; ?>
                            </div><!--/.single-teacher-->
                            <?php $counter++; ?>
                    <?php endforeach; ?>
                </div><!--/.teachers-block-->
            </div><!--/.container-->
        </div><!--/.teacher-training-block-->
    </div>
    <?php endif;?>
</article>
<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>
