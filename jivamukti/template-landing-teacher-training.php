<?php
/** Template Name: TT Landing Page */
get_header();
global $post; ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   
    <div id="post-<?php the_ID(); ?>" <?php post_class('template-teacher-training'); ?> >

        <hgroup class="template-title-group">
            <h1 class="block-title template-title brown"><?php the_title(); ?></h1>
        </hgroup>
        <?php if ( has_post_thumbnail() ) : ?>
            <div class="full-width-img">
                <?php the_post_thumbnail();?>
            </div>
            <?php $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
            <div class="wide-image" style="background-image: url(<?php echo $thumbnail[0]; ?>)"></div>
        <?php else: ?>
            <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" class="big-logo"></div>
        <?php endif; ?>
        <div class="container jv-container">
            <div class="entry-content">
                <?php the_content();?>
            </div>
            <div class="video-container">
                <div class="embed-responsive">
                    <?php the_field('video'); ?>
                </div>
            </div>
            <div class="pages-block">
                <?php
               $post_objects = array_merge(array(get_field('300_hr_certification_page')), array(get_field('800_hr_certification_page')),array(get_field('advance_certification_page')));
                if( $post_objects ):
                    foreach( $post_objects as $post): ?>
                        <?php setup_postdata($post); ?>
                        <?php if ( has_post_thumbnail() ) :
                            $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
                            <a href="<?php the_permalink(); ?>" class="single-page-rectangle" style="background-image: url(<?php echo $thumbnail[0]; ?>)">
                                <div class="page-rectangle-content page-rectangle-title">
                                    <?php the_title(); ?>
                                    <?php if(get_field('extra_info')): ?>
                                        <p><?php the_field('extra_info');?></p>
                                   <?php endif; ?>
                                </div>
                            </a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                <?php endif; ?>
            </div>
        </div>
        <?php $args = array(
            'post_type'			=> 'teacher-training',
            'posts_per_page'	=>  -1,
            'post_status'       => 'publish',
            'meta_query' => array(
                array(
                    'key' => 'begin',
                    'value'   => date('Ymd'),
                    'type'    => 'numeric',
                    'compare' => '>',
                ),
            ),
            'tax_query' => array(
                array(
                    'taxonomy' => 'teacher_training_categories',
                    'field'    => 'slug',
                    'terms'    => 'active',
                ),
            ),
            'meta_key'	=> 'begin',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
        );
        $the_query = new WP_Query( $args ); ?>
        <?php if ( $the_query->have_posts() ) : ?>
        <div class="container-fluid">
            <div class="row">
                <div class="upcoming-teacher-trainings">

                        <h2 class="content-title brown">Upcoming Teacher Trainings</h2>
                        <?php $counter = 0; ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <?php if ($counter % 3 == 0) {
                                $class = 'brown-tt';
                            } elseif ($counter % 3 == 1) {
                                $class = 'green-tt';
                            }
                            else {
                                $class = 'pink-tt';
                            } ?>
                            <div class="training-container">
                                <?php if ( has_post_thumbnail() ) :
                                    $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
                                    <div class="training-item" style="background-image: url(<?php echo $thumbnail[0]; ?>); order: <?php echo ($counter % 2 == 0) ? '0' : '1'?>"></div>
                                <?php else: ?>
                                    <div class="noimg-block" style="order: <?php echo ($counter % 2 == 0) ? '0' : '1'?>"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" class="big-logo"></div>
                                <?php endif; ?>

                                <div class="event-info-block <?php echo $class; ?>">
                                    <div class="event-info-content">
                                        <p class="event-info-header">Teacher Training</p>
                                        <p class="event-info-country">in <?php the_field('country'); ?></p>
                                        <p class="event-info-date"><?php echo date('F j', strtotime(get_field('begin'))); ?> - <?php the_field('end'); ?></p>
                                        <p class="event-info-hour"><?php the_field('hours'); ?> Hour</p>
                                        <a href="<?php the_permalink(); ?>" class="jv-btn btn-white btn-border">
                                            More info
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php $counter++; ?>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>

                </div>
                <div class="have-questions-block text-center">
                    <div class="have-questions-title post-type-title brown">if you have any questions</div>
                    <a href="<?php the_field('contact_us', 'option'); ?>" class="jv-btn btn-braun btn-border">Contact us</a>
                </div>

            </div>
        </div>
        <?php endif; ?>
    </div>

<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>





<?php get_footer(); ?>



