<?php
$accordion_fields = get_field('accordion_1');
if ($accordion_fields):
    foreach ($accordion_fields as $accordion_field) :
        if ($accordion_field['title'] != '' && $accordion_field['content'] != '') : ?>
            <?php $count = 0; ?>
            <div class="accodion-block">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php while (have_rows('accordion_1')) : the_row(); ?>
                        <?php if (get_sub_field('title') && get_sub_field('content')): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading<?php echo $count; ?>">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" role="button" data-toggle="collapse"
                                           href="#collapse<?php echo $count; ?>" aria-expanded="true"
                                           aria-controls="collapse<?php echo $count; ?>">
                                            <?php the_sub_field('title'); ?>
                                            <i class="indicator fa fa-caret-right pull-right"
                                               aria-hidden="true"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse<?php echo $count; ?>"
                                     class="panel-collapse collapse"
                                     role="tabpanel" aria-labelledby="heading<?php echo $count; ?>">
                                    <div class="panel-body">
                                        <div class="panel-content clearfix">
                                            <?php the_sub_field('content'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $count++; ?>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php break;
        endif;
    endforeach;
endif; ?>