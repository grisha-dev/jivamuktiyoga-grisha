<a class="blog-item" href="<?php the_permalink(); ?>">
    <?php if (has_post_thumbnail()) : ?>
        <div class="post-img" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large')[0]; ?>)"></div>
    <?php else: ?>
        <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>"
                                      alt=""></div>
    <?php endif; ?>
    <div class="post-info">
        <?php if(get_the_title()): ?>
            <h4 class="post-info-title"><?php the_title(); ?></h4>
        <?php endif; ?>
        <?php if(get_field('address')): ?>
            <div class="post-info-address" style="font-size: 13px; line-height: 20px;"><?php the_field('address'); ?></div>
        <?php endif; ?>



        <p class="post-info-date"><?php echo $post_date=''; ?></p>
    </div>

</a>