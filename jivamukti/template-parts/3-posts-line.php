<a class="post-item" href="<?php the_permalink(); ?>">
    <?php if ( has_post_thumbnail() ) : ?>
        <div class="post-img" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full')[0];?>)"></div>
    <?php else: ?>
        <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" ></div>
    <?php endif; ?>
    <div class="post-info">
        <h4 class="post-info-title"><?php the_title(); ?></h4>
        <p class="post-info-desc"><?php echo jv_excerpt(20); ?></p>
        <?php
        if (get_field('post_date')) :
            if (get_post_type() === 'fotm'):
                $post_date = date('F, Y', strtotime(get_field('post_date')));
            else:
                $post_date = date('F d, Y', strtotime(get_field('post_date')));
            endif;
        else:
            $post_date = get_the_date('F d, Y');
        endif; ?>
        <p class="post-info-date"><?php echo $post_date;?></p>

    </div>

</a>