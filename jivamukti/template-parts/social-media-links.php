<?php
$sm_links = get_field('opt_sm_links', 'options');
if ($sm_links) {
    echo '<ul class="sm-links">';
    foreach ($sm_links as $key => $item) {
//        var_dump($item['type']);
        echo '<li class="sm-links-item">';
        echo '<a class="sm-links-link" target="_blank" href="' . $item['url'] . '"><i class="sm-links-icon fa fa-' . $item['type'] . '" aria-hidden="true"></i>' . $item['type'] . '</a>';
        echo '</li>';
    }
    echo '</ul>';
}
?>