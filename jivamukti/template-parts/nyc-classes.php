<div class="training-container">
    <?php  $image = get_field('class_desc_image');
    if (!empty($image)): ?>
        <div class="training-item" style="background-image: url(<?php echo $image['sizes']['medium_large']; ?>); order: <?php echo ($counter % 2 == 0) ? '0' : '1'?>"></div>
    <?php else: ?>
        <div class="noimg-block" style="order: <?php echo ($counter % 2 == 0) ? '0' : '1'?>"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" class="big-logo"></div>
    <?php endif; ?>

    <div class="jv-classes-info-block event-info-block ">
        <div class="jv-classes-content event-info-content">
            <h3 class="jv-classes-header white"><?php the_title();?></h3>
            <p class="jv-classes-desc"><?php the_excerpt();?></p>
            <a class="readmore-white" href="<?php the_permalink(); ?>">Read more</a>
        </div>
    </div>
</div>
