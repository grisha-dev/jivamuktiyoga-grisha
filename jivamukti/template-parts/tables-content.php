<?php
foreach ($table['body'] as $tr) {
    echo '<tr>';
    foreach ($tr as $td) {
        if ($td['c']) {
            echo '<td>';
            if (has_shortcode($td['c'],'sign_up_button'))
                echo do_shortcode($td['c']);
            else
                echo $td['c'];
            echo '</td>';
        }
    }
    echo '</tr>';
}
?>