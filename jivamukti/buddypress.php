<?php
get_header();
global $post; ?>
    <div class="wide-layout 1">
        <div class="container-fluid">
            <div class="row">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
                        <hgroup class="title-group">
                            <h1 class="block-title brown"><?php the_title(); ?></h1>
                        </hgroup>
                            <?php the_content(); ?>
                    </div>
                <?php endwhile;
                else : ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>