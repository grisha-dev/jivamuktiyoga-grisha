<?php
/**
 * Template Name: Contact us
 */
get_header();
 ?>
<div class="contact-us-template">
    <div class="container-fluid">
        <div class="row">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
                    <hgroup class="template-title-group">
                        <h1 class="block-title template-title brown"><?php the_title(); ?></h1>
                    </hgroup>
                    <?php if ( has_post_thumbnail() ) : ?>
                        <div class="full-width-img">
                            <?php the_post_thumbnail();?>
                        </div>
                        <?php $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
                        <div class="wide-image" style="background-image: url(<?php echo $thumbnail[0]; ?>)"></div>
                    <?php else: ?>
                        <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" class="big-logo"></div>
                    <?php endif; ?>
                    <div class="container jv-container">
                        <div class="row">
                            <div class="col-md-6">
                                <?php if(get_field('contact_info_title')): ?>
                                    <h2 class="post-type-title brown"><?php the_field('contact_info_title')?></h2>
                                <?php endif; ?>
                                <div class="contact-info">
                                    <?php if(get_field('contact_info_description')): ?>
                                       <?php the_field('contact_info_description')?>
                                    <?php endif; ?>
                                    <?php if(get_field('opt_contact_data_adress','options')): ?>
                                        <p><strong>Address:</strong> <?php the_field('opt_contact_data_adress', 'options');?></p>
                                    <?php endif; ?>
                                    <?php if(get_field('opt_contact_data_phone','options')): ?>
                                        <p><strong>Phone:</strong> <?php the_field('opt_contact_data_phone', 'options');?></p>
                                    <?php endif; ?>
                                    <?php if(get_field('opt_contact_data_email','options')): ?>
                                        <p><strong>Email:</strong> <?php the_field('opt_contact_data_email', 'options');?></p>
                                    <?php endif; ?>
                                </div>
                            <?php get_template_part('template-parts/social-media-links'); ?>
                            </div>
                            <?php if(get_field('contact_form')): ?>
                                <div class="col-md-6">
                                    <h2 class="post-type-title brown">Contact us</h2>
                                    <div class="contact-form">
                                        <?php echo do_shortcode(get_field('contact_form')); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">

                            <div id="map"></div>
                        </div>
                    </div>

                </div>
            <?php endwhile;
            else : ?>
                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php $location = get_field('google_map'); ?>
<script type="text/javascript">



    if (document.getElementById('map')) {
        var center = {lat: <?php echo $location['lat']; ?>, lng: <?php echo $location['lng']; ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: center
        });
        var marker = new google.maps.Marker({
            position: center,
            map: map
        });
        google.maps.event.addDomListener(window, "resize", function () {
            var center = map.getCenter();
            google.maps.event.trigger(map, "resize");
            setTimeout(function () {
                map.setCenter(center);
            }, 100)
        });
    }



</script>
<?php get_footer(); ?>

