<?php
/**
 * Template Name: About
 */
get_header();
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div id="post-<?php the_ID(); ?>" <?php post_class('template-about'); ?> >
    <hgroup class="template-title-group">
        <h1 class="block-title template-title brown"><?php the_title(); ?></h1>
    </hgroup>
    <?php if ( has_post_thumbnail() ) : ?>
        <div class="full-width-img">
            <?php the_post_thumbnail();?>
        </div>
        <?php $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
        <div class="wide-image" style="background-image: url(<?php echo $thumbnail[0]; ?>)"></div>
    <?php else: ?>
        <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" class="big-logo"></div>
    <?php endif; ?>
    <section class="locations">
        <div class="container jv-container text-center">
        <h2 class="content-title text-center">Locations</h2>
        <?php
        $post_object = get_post('8084');
        if( $post_object ):
            // override $post
            $post = $post_object;
            setup_postdata( $post ); ?>
            <div class="main-location">
                <div class="image-text-line">

                        <div class="row">
                            <div class="col-md-6 itl-img-wrap">
                                <?php if ( has_post_thumbnail() ) : ?>
                                    <div class="itl-img" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'large')[0]; ?>);"></div>
                                <?php else: ?>
                                    <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" ></div>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-6">
                                <div class="itl-content">
                                <h3 class="itl-title">NYC Jivamukti Yoga Centers</h3>
                                    <?php the_field('address');?>
                                    <p><?php the_field('phone');?></p>
                                    <div class="brown-links">
                                        <?php get_template_part('template-parts/social-media-links'); ?>
                                    </div>
                                    <p class="itl-desc"><?php echo jv_excerpt(30, ' ...');?>
                                        <a href="<?php echo get_the_permalink(); ?>" class="read-more-btn">Read more</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
        <?php
        $posts = get_field('jivamukti_yoga_centers');
        if( $posts ): ?>
        <div class="jv-yoga-centers">

                <div class="posts-block-top">
                    <h3 class="posts-block-title">Jivamukti Yoga Centers</h3>
                    <a class="redirect-link more" href="<?php echo get_term_link('jivamukti-yoga-centers', 'locations_categories'); ?>">View all JIVAMUKTI YOGA CENTERS</a>
                </div>
                <div class="posts-container">
                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                        <a class="post-item" href="<?php the_permalink(); ?>">
                            <?php if ( has_post_thumbnail() ) : ?>
                                <div class="post-img" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full')[0];?>)"></div>
                            <?php else: ?>
                                <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" ></div>
                            <?php endif; ?>
                            <div class="post-info">
                                <h4 class="post-info-title"><?php the_title(); ?></h4>
                                <div class="post-info-address"><?php the_field('address');?></div>
                            </div>

                        </a>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                </div>

        </div>
        <?php endif;?>
        <?php
        $posts = get_field('jivamukti_affiliate_studios');
        if( $posts ): ?>
            <div class="jv-aff-studios">

                    <div class="posts-block-top">
                        <h3 class="posts-block-title">Jivamukti Affiliate Studios</h3>
                        <a class="redirect-link more" href="<?php echo get_term_link('jivamukti-affiliate-studios', 'locations_categories'); ?>">View all JIVAMUKTI AFFILIATE STUDIOS</a>
                    </div>
                    <div class="posts-container">
                        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                        <?php setup_postdata($post); ?>
                            <a class="post-item" href="<?php the_permalink(); ?>">
                                <?php if ( has_post_thumbnail() ) : ?>
                                    <div class="post-img" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full')[0];?>)"></div>
                                <?php else: ?>
                                    <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" ></div>
                                <?php endif; ?>
                                <div class="post-info">
                                    <h4 class="post-info-title"><?php the_title(); ?></h4>
                                    <div class="post-info-address"><?php the_field('address');?></div>
                                </div>

                            </a>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>

            </div>
        <?php endif;?>
        <a class="jv-btn btn-border btn-braun" href="<?php echo get_post_type_archive_link('locations');?>">View all locations</a>
        </div>
    </section>
    <section class="the-jv-method text-center">
            <h2 class="jv-method-title"> The Jivamukti Method</h2>
            <div class="jv-method-desc"><?php the_field('the_jivamukti_method_description');?></div>
            <a class="jv-btn btn-border btn-braun" href="<?php the_field('the_jivamukti_method_page', 'options');?>">Learn The Jivamukti Method</a>
    </section>
    <?php
    global $wpdb;
    $ids_str = '';
    $counter = 0;
    $city_field_id = xprofile_get_field_id_from_name('City');
    $user_ids = $wpdb->get_results("SELECT a.user_id FROM " . $wpdb->prefix . "bp_xprofile_data a, " . $wpdb->prefix . "users b, " . $wpdb->prefix . "term_relationships tm WHERE a.user_id = b.ID AND a.user_id = tm.object_id AND a.field_id = " . $city_field_id . " AND a.value = 'New York' AND b.user_status = 0 AND tm.term_taxonomy_id = 1259 ORDER BY RAND()"); ?>
    <?php if($user_ids): ?>
    <section class="nyc-teachers-block padding-block">
        <hgroup class="title-group">
            <h1 class="block-title brown">Jivamukti NYC Teachers</h1>
        </hgroup>
        <div class="container text-center">
            <div class="teachers-block">
                <?php foreach ($user_ids as $id):?>

                        <?php $avatar_url = bp_core_fetch_avatar(
                            array(
                                'item_id' => $id->user_id, // id of user for desired avatar
                                'type' => 'full',
                                'html' => FALSE     // FALSE = return url, TRUE (default) = return img html
                            )
                        ); ?>
                        <div class="single-teacher">
                            <div class="teacher-info">
                                <a href="<?php echo bp_core_get_user_domain($id->user_id); ?>" class="teacher-img" style="background-image: url(<?php echo $avatar_url; ?>)"></a>
                                <!--/.teacher-img-->
                                <p class="teacher-name"><a href="<?php echo bp_core_get_user_domain($id->user_id); ?>"> <?php echo bp_core_get_core_userdata($id->user_id)->display_name; ?></a></p><!--/.teacher-name-->
                            </div><!--/.teacher-info-->
                            <?php
                            if (xprofile_get_field_data('Certificate',$id->user_id)): ?>
                                <p class="teacher-hour"><?php echo xprofile_get_field_data('Certificate',$id->user_id); ?>
                                    Certified</p><!--/.teacher-hour-->
                            <?php endif; ?>
                        </div><!--/.single-teacher-->
                        <?php $counter++; ?>
                        <?php  if($counter == 8) break; ?>

                <?php endforeach; ?>
            </div><!--/.teachers-block-->
            <?php bp_member_hidden_fields(); ?>
            <a href="<?php the_field('teachers', 'options'); ?>" class="jv-btn btn-border btn-braun">View all teachers</a>
        </div><!--/.container-->
    </section><!--/.teacher-training-block-->
    <?php endif;?>
    <?php
    $post_object = get_field('sharon_david_page');
    if( $post_object ):
    // override $post
    $post = $post_object;
    setup_postdata( $post ); ?>
    <section class="about-sharon-david">
        <div class="container jv-container text-center">
            <h2 class="content-title white">About Sharon Gannon & David Life</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="ash-image" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'large')[0]; ?>);"></div>
                </div>
                <div class="col-md-6">
                    <div class="ash-desc">
                        <?php the_excerpt() ?>
                        <a class="ash-readmore" href="<?php the_permalink(); ?>">Read more</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php $table = get_field( 'jivamukti_over_the_years_table' );
                    for ($i = 0; $i < count($table['body']); $i++ ) {
                        for ($j = 0; $j < count($table['body'][$i]); $j++) {
                            if (!empty($table['body'][$i][$j]['c'])) { ?>
                                <h3 class="block-title-21 white">Jivamukti Over the Years</h3>
                                <div class="table-wrap">
                                    <table class="table asd-table">
                                        <?php include(locate_template('template-parts/tables-content.php',false,false)); ?>
                                    </table>
                                </div>
                                <?php break 2; ?>

                            <?php }
                        }
                    }?>
                </div>
                <a class="jv-btn btn-border btn-white" href="<?php the_permalink(); ?>">More info</a>
            </div>
        </div>
    </section>
        <?php wp_reset_postdata(); ?>
    <?php endif; ?>
    <?php
    $post_object = get_field('prices_page');
    if( $post_object ):
    // override $post
    $post = $post_object;
    setup_postdata( $post ); ?>
    <section class="prices text-center">
        <h2 class="text-center">Prices</h2>
        <div class="container white-bg">
            <div class="row">
                <?php $table = get_field('drop-ins_and_memberships');
                for ($i = 0; $i < count($table['body']); $i++) {
                    for ($j = 0; $j < count($table['body'][$i]); $j++) {
                        if (!empty($table['body'][$i][$j]['c'])) { ?>
                            <div class="col-md-6">
                                <h3>Drop-ins and Memberships</h3>
                                <div class="table-wrap">
                                    <table class="table price-table">
                                        <?php include(locate_template('template-parts/tables-content.php',false,false)); ?>
                                    </table>
                                </div>
                            </div>
                            <?php break 2; ?>
                        <?php }
                    }
                } ?>
                <?php $table = get_field('class_packs');
                for ($i = 0; $i < count($table['body']); $i++) {
                    for ($j = 0; $j < count($table['body'][$i]); $j++) {
                        if (!empty($table['body'][$i][$j]['c'])) { ?>
                            <div class="col-md-6">
                                <h3>Class Packs</h3>
                                        <div class="table-wrap">
                                            <table class="table price-table">
                                                <?php include(locate_template('template-parts/tables-content.php',false,false)); ?>
                                            </table>
                                        </div>
                                    </div>

                            <?php break 2; ?>
                        <?php }
                    }
                } ?>

            </div>
        </div>


            <a href="<?php the_permalink(); ?>" class="jv-btn btn-braun btn-border">View all prices</a>
        </div>
    </section>
        <?php wp_reset_postdata(); ?>
    <?php endif; ?>
        <section class="have-questions-block text-center">
            <div class="have-questions-title post-type-title brown">if you have any questions</div>
            <a href="<?php the_field('contact_us', 'option'); ?>" class="jv-btn btn-braun btn-border">Contact us</a>
        </section>
</div>

<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>
