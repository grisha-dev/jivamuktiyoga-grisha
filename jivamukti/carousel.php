<section class="slider carousel slide slider-container" id="myCarousel" >
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php
        $i=0;
        while( have_rows('carousel') ): the_row();
            if ($i == 0) {
                echo '<li data-target="#myCarousel" data-slide-to="0" class="active"></li>';
            } else {
                echo '<li data-target="#myCarousel" data-slide-to="'.$i.'"></li>';
            }
            $i++;
        endwhile; ?>
    </ol>
    <!-- Wrapper for Slides -->
    <div class="carousel-inner">
        <?php
        $z = 0;
        while( have_rows('carousel') ): the_row();
            $image = get_sub_field('single_slide');
            ?>
            <div class="item slides <?php if ($z==0) { echo 'active';} ?>">
                <div class="fill" style="background-image: url('<?php echo $image['url']; ?>')"></div>
                <div class="carousel-caption">
                    <div class="caption">
                        <h1><?php echo get_sub_field('slide_caption'); ?></h1>
                        <?php if (get_sub_field('button_text')) { ?>
                            <a class="jv-btn btn-border btn-white slider-btn" href="<?php echo get_sub_field('slider_link'); ?>"><?php echo get_sub_field('button_text'); ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php
            $z++;
        endwhile; ?>
        <a class="left carousel-control" data-target="#myCarousel" data-slide="prev">
            <img src="<?php echo get_template_directory_uri();?>/images/left_arrow.png">
        </a>
        <a class="right carousel-control" data-target="#myCarousel" data-slide="next">
            <img src="<?php echo get_template_directory_uri();?>/images/right_arrow.png">
        </a>
    </div>
</section>