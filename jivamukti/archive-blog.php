<?php
get_header(); ?>
<div class="arhive-blog">
    <hgroup class="template-title-group">
        <h1 class="block-title template-title brown"><?php echo (get_post_type() == 'fotm') ? 'Focus of the Month' : post_type_archive_title(); ?></h1>
    </hgroup>
    <section class="focus-moment-block">
        <div class="container">
            <div class="row">
                <?php
                // the query
//                $begin = date('Ym') . '01';
//                $end = date('Ym') . '31';
                $args = array(
                    'post_type' => get_post_type(),
                    'posts_per_page' => 1,
                    'post_status' => 'publish',
                    'meta_query' => array(
                        array(
                            'key'     => 'post_date',
                            'value'   => date('Ymd'),
                            'type'    => 'numeric',
                            'compare' => '<=',
                        ),
                    ),
                    'meta_key' => 'post_date',
                    'orderby' => 'meta_value_num',
                    'order' => 'DESC'
                );
//                if (get_post_type() == 'fotm') {
//                    $args['meta_query'] = array(
//                        'relation' => 'AND',
//                        array(
//                            'key' => 'post_date',
//                            'value' => array($begin, $end),
//                            'type' => 'numeric',
//                            'compare' => 'BETWEEN',
//                        ),
//                        array(
//                            'key'     => 'post_date',
//                            'value'   => date('Ymd'),
//                            'type'    => 'numeric',
//                            'compare' => '<=',
//                        ),
//                    );
//                }
//                var_dump($args);
                $the_query = new WP_Query($args); ?>
                <?php if ($the_query->have_posts()) : ?>
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <div class="col-xs-12 col-sm-6">
                            <div class="image-block"
                                 style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full')[0]; ?>)">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="description-block">
                                <div class="date-moment">
                                    <p class="date-month"><?php echo date('F, Y'); ?></p>
                                </div>
                                <hgroup class="title-group">
                                    <h2 class="content-title brown text-left"><?php the_title(); ?></h2>
                                </hgroup>
                                <?php the_excerpt(); ?>
                                <a href="<?php echo get_the_permalink(); ?>" class="read-more-btn">Read more</a>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <?php
    $ppp = 9;
    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    $offset_val = 1;
    $offset = ($paged == 1) ? $offset_val : $offset_val + ( ($paged  - 1) * $ppp );
    $args = array(
        'post_type' => get_post_type(),
        'posts_per_page' => $ppp,
        'offset' => $offset,
        'post_status' => 'publish',
        'meta_query' => array(
            array(
                'key'     => 'post_date',
                'value'   => date('Ymd'),
                'type'    => 'numeric',
                'compare' => '<=',
            ),
        ),
        'meta_key' => 'post_date',
        'orderby' => 'meta_value_num',
        'order' => 'DESC',
        'paged' => $paged
    );
    $the_query = new WP_Query($args);?>
    <?php if ($the_query->have_posts()) : ?>
        <section class="blog-posts">
            <div class="container jv-container">
                <div class="blog-container">
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <a <?php post_class('blog-item'); ?> href="<?php the_permalink(); ?>">
                            <?php if (has_post_thumbnail()) : ?>
                                <div class="post-img"
                                     style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'large')[0]; ?>)"></div>
                            <?php else: ?>
                                <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="">
                                </div>
                            <?php endif; ?>
                            <div class="post-info">
                                <h4 class="post-info-title"><?php the_title(); ?></h4>
                                <p class="post-info-desc"><?php echo jv_excerpt(20); ?></p>
                                <?php
                                if (get_field('post_date')) :
                                    if (get_post_type() === 'fotm'):
                                        $post_date = date('F, Y', strtotime(get_field('post_date')));
                                    else:
                                        $post_date = date('F d, Y', strtotime(get_field('post_date')));
                                    endif;
                                else:
                                    $post_date = get_the_date('F d, Y');
                                endif; ?>
                                    <p class="post-info-date"><?php echo $post_date;?></p>

                            </div>
                        </a>
                    <?php endwhile; ?>
                </div>
                    <div class="pagination-links">

                        <?php

                        $big = 999999999; // need an unlikely integer
                        echo paginate_links(array(
                            'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                            'format' => '?paged=%#%',
                            'current' => max(1, get_query_var('paged')),
                            'total' => ($the_query->found_posts - $offset_val) / $ppp
                        ));
                        ?>
                    </div>
                    <?php wp_reset_postdata(); ?>

            </div>
        </section>
</div>

<?php endif;
get_footer();
?>