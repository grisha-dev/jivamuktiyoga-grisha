<?php
/**
 * BuddyPress - Members Loop
 *
 * Querystring is set via AJAX in _inc/ajax.php - bp_legacy_theme_object_filter()
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

/**
 * Fires before the display of the members loop.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_members_loop' ); ?>

<?php if ( bp_get_current_member_type() ) : ?>
	<p class="current-member-type"><?php bp_current_member_type_message() ?></p>
<?php endif; ?>

<?php if (bp_has_members(bp_ajax_querystring('members'))) : ?>



<?php do_action('bp_before_directory_members_list'); ?>

<div class="container text-center">
	<div class="teachers-block">

		<?php while (bp_members()) : bp_the_member();

			$avatar_url = bp_core_fetch_avatar(
				array(
					'item_id' => bp_get_member_user_id(), // id of user for desired avatar
					'type' => 'full',
					'html' => FALSE     // FALSE = return url, TRUE (default) = return img html
				)
			); ?>
			<div class="single-teacher">
				<div class="teacher-info">
					<a href="<?php bp_member_permalink(); ?>" class="teacher-img"
					   style="background-image: url(<?php echo $avatar_url; ?>)"></a>
					<p class="teacher-name"><a href="<?php bp_member_permalink(); ?>"> <?php bp_member_name(); ?></a>
					</p>
				</div>
				<?php if (bp_get_member_profile_data('field=Certificate')): ; ?>
					<p class="teacher-hour"><?php bp_member_profile_data('field=Certificate'); ?>
						certified</p><!--/.teacher-hour-->
				<?php endif; ?>
			</div>
			<div class="action">
				<?php do_action('bp_directory_members_actions'); ?>
			</div>
		<?php endwhile; ?>
	</div>

	<?php do_action('bp_after_directory_members_list'); ?>

	<?php bp_member_hidden_fields(); ?>


	<div class="pagination-links" id="member-dir-pag-bottom">

		<?php bp_members_pagination_links(); ?>


	</div>

	<?php else: ?>

		<div id="message" class="info">
			<p><?php _e("Sorry, no members were found.", 'buddypress'); ?></p>
		</div>

	<?php endif; ?>
</div>



<?php

/**
 * Fires after the display of the members loop.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_members_loop' ); ?>
