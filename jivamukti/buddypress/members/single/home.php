<div class="page-titles">
    <div class="container jv-container">
        <a href="<?php echo get_field('teachers', 'options') ?>" class="redirect-link back">
            back to jivamukti teachers
        </a>
        <hgroup class="title-group">
            <h2 class="post-type-title brown text-center">Jivamukti Certified Teacher</h2>
            <h1 class="block-title brown"><?php the_title(); ?></h1>
        </hgroup>
    </div>
</div>

<div class="single-teacher-block">
    <div class="container jv-container">
        <div class="row">
            <div class="col-sm-5 col-md-5 teacher-avatar-wrap">

                <?php    $avatar_url =  bp_core_fetch_avatar (
                array(
                'item_id' => bp_displayed_user_id(), // id of user for desired avatar
                'type'    => 'full',
                'html'   => FALSE     // FALSE = return url, TRUE (default) = return img html
                )
                );?>
                <div class="teacher-avatar" style="background-image: url(<?php echo $avatar_url;?>);"></div>
            </div>
            <div class="col-sm-7 col-md-7 teacher-info-wrap">
                <div class="teacher-info">
                    <?php
                    $user = get_userdata(bp_displayed_user_id());
//                    var_dump($user);
                    if (bp_get_member_profile_data( 'field=Certificate' )): ;?>
                        <h3 class="teacher-hours post-type-title brown"><?php bp_member_profile_data( 'field=Certificate' );?> Certified</h3>
                    <?php endif; ?>
                    <?php if (bp_get_member_profile_data( 'field=City' )): ?>
                        <p class="teacher-city"><?php bp_member_profile_data( 'field=City' );
                            if (bp_get_member_profile_data( 'field=State/Province' )): ?>,
                                <?php bp_member_profile_data( 'field=State/Province' );?></p>
                            <?php endif; ?>
                    <?php endif; ?>
                    <?php if (bp_get_member_profile_data( 'field=Country' )): ;?>
                        <p class="teacher-country"><?php bp_member_profile_data( 'field=Country' );?></p>
                    <?php endif; ?>
                    <?php if (bp_get_member_profile_data( 'field=Website' )): ;?>
                        <p class="teacher-site"><a href="<?php bp_member_profile_data( 'field=Website' );?>"><?php echo preg_replace('#^https?://#', '', bp_get_member_profile_data( 'field=Website' ));?></a> </p>
                    <?php endif; ?>
                    <?php if (bp_get_member_profile_data('field=Show email on profile page')): ?>
                        <p class="teacher-email"><a href="mailto:<?php echo bp_get_displayed_user_email();?>"><?php echo bp_get_displayed_user_email();?></a></p>
                    <?php endif; ?>
                    <?php if (bp_get_member_profile_data( 'field=Phone' )): ;?>
                        <p class="teacher-phone"><a href="tel:<?php bp_member_profile_data( 'field=Phone' ); ?>"><?php bp_member_profile_data( 'field=Phone' );?></a></p>
                    <?php endif; ?>
                    <?php if (bp_get_member_profile_data( 'field=Facebook' ) || bp_get_member_profile_data( 'field=Twitter' ) || bp_get_member_profile_data( 'field=Pinterest' ) || bp_get_member_profile_data( 'field=Instagram' )): ?>
                    <ul class="sm-links">
                        <?php if (bp_get_member_profile_data( 'field=Facebook' )): ?>
                            <li class="sm-links-item">
                               <a class="sm-links-link" target="_blank" href="<?php bp_member_profile_data( 'field=Facebook' );?>"><i class="sm-links-icon fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if (bp_get_member_profile_data( 'field=Twitter' )): ?>
                            <li class="sm-links-item">
                                <a class="sm-links-link" target="_blank" href="<?php bp_member_profile_data( 'field=Twitter' );?>"><i class="sm-links-icon fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if (bp_get_member_profile_data( 'field=Pinterest' )): ?>
                            <li class="sm-links-item">
                                <a class="sm-links-link" target="_blank" href="<?php bp_member_profile_data( 'field=Pinterest' );?>"><i class="sm-links-icon fa fa-pinterest" aria-hidden="true"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if (bp_get_member_profile_data( 'field=Instagram' )): ?>
                            <li class="sm-links-item">
                                <a class="sm-links-link" target="_blank" href="<?php bp_member_profile_data( 'field=Instagram' );?>"><i class="sm-links-icon fa fa-instagram" aria-hidden="true"></i></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php  if (bp_get_member_profile_data( 'field=Bio' )): ;?>
<div class="entry-content">
    <div class="container jv-container">
        <h3 class="post-type-title brown">About <?php  echo bp_get_user_firstname() ; ?> </h3>
        <?php echo wpautop(bp_get_member_profile_data( 'field=Bio' ));?>
    </div>
</div>
<?php endif; ?>
<?php if (bp_get_member_profile_data( 'field=Healcode ID' )): ?>
<div class="teacher-classes">
    <div class="container jv-container">

        <div class="healcode-wrap">
            <div class="schedule-top">
                <h2 class="content-title brown">Classes by <?php bp_displayed_user_fullname(); ?></h2>
                <a href="http://site.com" class="buy-classes jv-btn-green">Buy classes</a>
            </div>
            <healcode-widget data-type="schedules" data-widget-partner="mb" data-widget-id="<?php echo bp_get_member_profile_data( 'field=Healcode ID' );?>" data-widget-version="0.1"></healcode-widget>
        </div>
    </div>
</div>
<?php endif; ?>