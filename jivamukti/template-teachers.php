<?php
/** Template Name: Teachers */
get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="bp-teachers-directory" >
        <div class="container jv-container">
            <hgroup class="title-group">
                <h1 class="block-title brown"><?php the_title(); ?></h1>
            </hgroup>
            <?php echo do_shortcode('[bps_display form=9764]'); ?>
        </div>
        <?php do_action( 'bp_before_members_loop' ); ?>

        <?php if (bp_has_members( bp_ajax_querystring('members'))) : ?>
            <?php do_action('bp_before_directory_members_list'); ?>
            <div class="container text-center">
                <div class="teachers-block">
                    <?php while (bp_members()) : bp_the_member();

                            $avatar_url = bp_core_fetch_avatar(
                                array(
                                    'item_id' => bp_get_member_user_id(), // id of user for desired avatar
                                    'type' => 'full',
                                    'no_grav' => false,
                                    'html' => FALSE     // FALSE = return url, TRUE (default) = return img html
                                )
                            ); ?>
                            <div class="single-teacher">
                                <div class="teacher-info">
                                    <a href="<?php bp_member_permalink(); ?>" class="teacher-img" style="background-image: url(<?php echo $avatar_url; ?>)"></a>
                                    <p class="teacher-name"><a href="<?php bp_member_permalink(); ?>"> <?php bp_member_name(); ?></a></p>
                                </div>
                                <?php if (bp_get_member_profile_data('field=Certificate')): ; ?>
                                    <p class="teacher-hour"><?php bp_member_profile_data('field=Certificate'); ?>
                                        Certified</p><!--/.teacher-hour-->
                                <?php endif; ?>
                                <?php $teacher_location_str = ''?>
                                <?php if (bp_get_member_profile_data('field=City') && empty($_GET['field_28_contains']) && !empty($_GET)): ; ?>
                                    <?php $teacher_location_str = bp_get_member_profile_data('field=City');?>
                                    <?php if (bp_get_member_profile_data('field=Country')):
                                        $teacher_location_str .= ', ' . bp_get_member_profile_data('field=Country');
                                    endif; ?>
                                    <p class="teacher-location"><?php echo $teacher_location_str; ?></p>
                                <?php endif; ?>
                            </div>

                    <?php endwhile; ?>
                </div>

                <?php do_action('bp_after_directory_members_list'); ?>

                <?php bp_member_hidden_fields(); ?>

                <?php if (bp_get_members_pagination_links()): ?>
                    <div class="pagination-links" id="member-dir-pag-bottom">
                        <?php bp_members_pagination_links(); ?>
                    </div>
                <?php endif;?>
            </div>
        <?php else: ?>

            <div id="message" class="info text-center">
                <h2><?php _e("Sorry, no teachers were found.", 'buddypress'); ?></h2>
                <p class="no-teachers-desc"><?php _e("Please try different search criteria", 'buddypress'); ?></p>
            </div>

        <?php endif; ?>




        <?php

        /**
         * Fires after the display of the members loop.
         *
         * @since 1.2.0
         */
        do_action( 'bp_after_members_loop' ); ?>


    </div>
<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
