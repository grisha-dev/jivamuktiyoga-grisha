<?php
$counter = 0;
for ($i = date('Y'); $i > 0; $i--) {
    echo '$i' . $i . '<br>';
    for ($j = $i . '12'; $j >= $i . '01'; $j-- ) {
        $begin = $i . $j . '01';
        $end = $i . $j . '31';
        $args = array(
            'post_type' => 'fotm',
            'posts_per_page' => 1,
            'post_status' => 'publish',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key'     => 'post_date',
                    'value'   => array($begin,$end),
                    'type'    => 'numeric',
                    'compare' => 'BETWEEN',
                ),
                array(
                    'key'     => 'post_date',
                    'value'   => date('Ymd'),
                    'type'    => 'numeric',
                    'compare' => '<=',
                ),
            ),
            'meta_key' => 'post_date',
            'orderby' => 'meta_value_num',
            'order' => 'DESC'
        );
        $the_query = new WP_Query( $args ); ?>
        <?php if ( $the_query->have_posts() ) : ?>
            <section class="fotm-posts">
                <div class="container jv-container">
                    <div class="posts-block-top">
                        <h3 class="posts-block-title">Focus Of The Month</h3>
                        <a class="redirect-link more" href="<?php echo get_post_type_archive_link($args['post_type']); ?>">View all FOTMs</a>
                    </div>
                    <div class="posts-container">
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <a class="post-item" href="<?php the_permalink(); ?>">
                                <?php if ( has_post_thumbnail() ) : ?>
                                    <div class="post-img" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full')[0];?>)"></div>
                                <?php else: ?>
                                    <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" ></div>
                                <?php endif; ?>
                                <div class="post-info">
                                    <p class="post-info-title"><?php the_title(); ?></p>
                                    <p class="post-info-desc"><?php echo jv_excerpt(20); ?></p>
                                    <?php if(get_field('post_date')): ?>
                                        <p class="post-info-date"><?php echo date('F, Y', strtotime(get_field('post_date')));?></p>
                                    <?php endif; ?>
                                </div>

                            </a>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </section>
        <?php endif; ?>
<?php
//        echo '$j' . $j.'<br>';
//        $counter++;
//        if ($counter == 40) break 2;
    }
}




