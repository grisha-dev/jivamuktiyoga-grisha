<?php

get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class('template-landing-blog'); ?> >
        <hgroup class="template-title-group">
            <h1 class="block-title template-title brown"><?php the_title(); ?></h1>
        </hgroup>
        <section class="focus-moment-block padding-block">
            <div class="container">
                <div class="row">
                    <?php
                    // the query

                    $begin = date('Ym') . '01';
                    $end = date('Ym') . '31';
                    $args = array(
                        'post_type'			=> 'fotm',
                        'posts_per_page'	=>  1,
                        'post_status'       => 'publish',
                        'meta_query' => array(
                            array(
                                'key' => 'post_date',
                                'value'   => array( $begin, $end ),
                                'type'    => 'numeric',
                                'compare' => 'BETWEEN',
                            ),
                        ),
                        'meta_key'	=> 'post_date',
                        'orderby' => 'meta_value_num'
                    );
                    $the_query = new WP_Query( $args ); ?>
                    <?php if ( $the_query->have_posts() ) : ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <div class="col-xs-12 col-sm-6">
                                <div class="image-block" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full')[0]; ?>)">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="description-block">
                                    <div class="date-moment">
                                        <p class="date-month">Focus of the Month: <?php echo date('F, Y');?></p>
                                    </div>
                                    <hgroup class="title-group">
                                        <div class="content-title brown text-left"><?php the_title(); ?></div>
                                    </hgroup>
                                    <p>
                                        <?php the_excerpt(); ?>
                                    </p>
                                    <a href="<?php echo get_the_permalink(); ?>" class="read-more-btn">Read more</a>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </div>
    <?php
endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>