<?php
get_header();
?>

<section class="locations">
    <div class="container jv-container">
        <hgroup class="template-title-group">
            <h1 class="block-title template-title brown"><?php post_type_archive_title(); ?></h1>
        </hgroup>
        <?php
        $post_object = get_post('8084');
        if ($post_object):
            // override $post
            $post = $post_object;
            setup_postdata($post); ?>
            <div class="main-location">
                <div class="image-text-line">
                    <div class="row">
                        <div class="col-md-6 itl-img-wrap">
                            <?php if (has_post_thumbnail()) : ?>
                                <div class="itl-img"
                                     style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'large')[0]; ?>);"></div>
                            <?php else: ?>
                                <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>"
                                                              alt=""></div>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6">
                            <div class="itl-content">
                                <h3 class="itl-title">NYC Jivamukti Yoga Centers</h3>
                                <p><?php the_field('address'); ?></p>
                                <p><?php the_field('phone'); ?></p>
                                <div class="brown-links">
                                    <?php get_template_part('template-parts/social-media-links'); ?>
                                </div>
                                <p class="itl-desc"><?php echo jv_excerpt(30, ' ...'); ?>
                                    <a href="<?php echo get_the_permalink(); ?>" class="read-more-btn">Read more</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
        <?php
        $args = array(
            'post_type' => 'locations',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'locations_categories',
                    'field' => 'slug',
                    'terms' => 'jivamukti-yoga-centers',
                ),
            ),
            'orderby' => 'title',
            'order' => 'ASC',
        );
        $the_query = new WP_Query($args); ?>
        <?php if ($the_query->have_posts()): ?>
            <div class="jv-yoga-centers">
                <div class="posts-block-top">
                    <h3 class="posts-block-title">Jivamukti Yoga Centers</h3>
                </div>
                <div class="blog-container">
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                       <?php get_template_part('template-parts/blog-posts') ?>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php
        $args = array(
            'post_type' => 'locations',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'locations_categories',
                    'field' => 'slug',
                    'terms' => 'jivamukti-affiliate-studios',
                ),
            ),
            'orderby' => 'title',
            'order' => 'ASC',
        );
        $the_query = new WP_Query($args); ?>
        <?php if ($the_query->have_posts()): ?>
        <div class="jv-aff-studios">
            <div class="posts-block-top">
                <h3 class="posts-block-title">JIVAMUKTI AFFILIATE STUDIOS</h3>
            </div>
            <div class="blog-container">
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <?php get_template_part('template-parts/blog-posts') ?>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php get_footer(); ?>
