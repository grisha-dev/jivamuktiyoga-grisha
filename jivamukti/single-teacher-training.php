<?php
get_header();
global $post; ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
        <div class="container jv-container">
            <div class="page-titles">
                <a href="<?php the_field('teacher_training_page', 'options') ?>" class="redirect-link back">
                    Back to jivamukti teacher training
                </a>
                <hgroup class="title-group">
                    <h3 class="post-type-title brown text-center">Teacher Training</h3>
                    <h1 class="block-title brown"><?php the_title(); ?></h1>
                </hgroup>
            </div>
            <div class="training-top-block">
                <div class="gallery">
                    <?php
                    $images = get_field('image_gallery');
                    if( $images ):
                        $count = 0;
                        foreach( $images as $image ):
                            if ($count == 0) : ?>
                            <div class="single-image main" style="background-image: url(<?php echo $image['url']; ?>);"></div>
                                <div class="thumbnails-column">
                            <?php endif; ?>
                                    <div class="single-image" style="background-image: url(<?php echo $image['url']; ?>);"></div>
                            <?php
                            if (($count == count($images) - 1)) : ?>
                            </div>
                            <?php endif;
                            $count++; ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" ></div>
                    <?php endif; ?>
                </div>
                    <div class="sign-up-block">
                        <p class="training-date-from"><span>From: </span><?php the_field('begin'); ?></p>
                        <p class="training-date-to"><span>To: </span><?php the_field('end'); ?></p>
                        <p class="training-location"><span>Location: </span><?php the_field('location'); ?>, <?php the_field('country'); ?></p>
                        <a href="<?php the_field('prerequisites_page'); ?>" class="redirect-link more">prerequisites</a>
                        <a href="<?php the_field('sign_up_url'); ?>" class="jv-btn-green btn-sign-up">Register</a>
                    </div>
            </div>
            <div class="entry-content">
                <?php the_content();?>
            </div>
            <?php $tables = array_merge(array(get_field('dates_and_deadlines_table')),array(get_field('daily_schedule_table')));
            $flag = false;
            foreach ($tables as $table) {
                for ($i = 0; $i < count($table['body']); $i++ ) {
                    for ($j = 0; $j < count($table['body'][$i]); $j++ ) {
//                        var_dump($table['body'][$i][$j]['c']);
                        if ($table['body'][$i][$j]['c'] != ''){
                            $flag = true;
                        }
                    }
                }
            }
            if ($flag): ?>
                <div class="training-tables">
                    <div class="row">
                        <?php $table = get_field( 'dates_and_deadlines_table' );
                        $flag = false;
                        for ($i = 0; $i < count($table['body']); $i++ ) {
                            for ($j = 0; $j < count($table['body'][$i]); $j++ ) {
                                if ($table['body'][$i][$j]['c'] != ''){
                                    $flag = true;
                                }
                            }
                        };?>
                        <?php if ($flag): ?>
                            <div class="col-md-6">
                                <h3 class="post-type-title brown">Dates and Deadlines</h3>
                                <div class="table-wrap">
                                    <table class="table">
                                        <?php
                                        echo '<tbody>';
                                            include(locate_template('template-parts/tables-content.php',false,false));
                                        echo '</tbody>';
                                        ?>
                                    </table>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php $table = get_field( 'daily_schedule_table' );
                        $flag = false;
                        for ($i = 0; $i < count($table['body']); $i++ ) {
                            for ($j = 0; $j < count($table['body'][$i]); $j++ ) {
                                if ($table['body'][$i][$j]['c'] != ''){
                                    $flag = true;
                                }
                            }
                        };?>
                        <?php if ($flag): ?>
                            <div class="col-md-6">
                                <h3 class="post-type-title brown">Daily Schedule</h3>
                                <div class="table-wrap">
                                    <table class="table">
                                        <?php
                                        echo '<tbody>';
                                        include(locate_template('template-parts/tables-content.php',false,false));
                                        echo '</tbody>';
                                        ?>
                                    </table>
                                </div>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
            <?php endif;?>
            <?php
                    $flag = false;
                    $accordion_fields = get_field('accordion_1');
                    for ($i = 0; $i < count($accordion_fields); $i++ ) {
                        if (get_field('accordion_1')[$i]['title'] != '' && get_field('accordion_1')[$i]['content'] != '') {
                            $flag = true;
                        }
                    }
                    if ($flag) :
                    $count = 0; ?>
                    <div class="planning-trip-block">
                        <h3 class="post-type-title brown">Planning Your Trip</h3>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php while (have_rows('accordion_1')) : the_row(); ?>
                            <?php if(get_sub_field('title') && get_sub_field('content')): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading<?php echo $count; ?>">
                                    <h4 class="panel-title">

                                        <a class="accordion-toggle" role="button" data-toggle="collapse"  href="#collapse<?php echo $count; ?>" aria-expanded="true" aria-controls="collapse<?php echo $count; ?>" >
                                            <?php the_sub_field('title'); ?>
                                            <i class="indicator fa fa-caret-right pull-right" aria-hidden="true"></i>
                                        </a>

                                    </h4>
                                </div>
                                <div id="collapse<?php echo $count; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $count; ?>">
                                    <div class="panel-body">
                                         <div class="panel-content clearfix">
                                           <?php the_sub_field('content'); ?>
                                         </div>
                                    </div>
                                </div>
                            </div>
                                <?php $count++;?>
                            <?php endif; ?>
                        <?php endwhile;?>
                        </div>
                    </div>
                    <?php endif; ?>


        </div>
        <div class="botttom-sign-up-block text-center">
            <h3 class="post-type-title brown"><?php the_title();?></h3>
            <p class="training-hour"><?php the_field('hours'); ?> Hour</p>
            <p class="training-date"><?php the_field('begin'); ?> – <?php the_field('end'); ?></p>
            <a href="<?php the_field('sign_up_url'); ?>" class="jv-btn-green btn-sign-up">Register</a>
        </div>
        <?php if(get_field('faculty')): ?>
            <div class="faculty">
                <?php
                //        var_dump(get_field('teachers'));
                $teaches = get_field('faculty'); ?>
                <div class="nyc-teachers-block">
                    <div class="container jv-container">
                        <h3 class="post-type-title brown faculty-title">Faculty</h3>
                    </div>
                    <div class="container text-center">
                        <div class="teachers-block faculty-teachers">
                            <?php foreach ($teaches as $teacher): ?>
                                    <?php $avatar_url = bp_core_fetch_avatar(
                                        array(
                                            'item_id' => $teacher['ID'], // id of user for desired avatar
                                            'type' => 'full',
                                            'html' => FALSE     // FALSE = return url, TRUE (default) = return img html
                                        )
                                    ); ?>
                                    <div class="single-teacher">
                                        <div class="teacher-info">
                                            <a href="<?php echo bp_core_get_user_domain($teacher['ID']); ?>" class="teacher-img" style="background-image: url(<?php echo $avatar_url; ?>)"></a>
                                            <!--/.teacher-img-->
                                            <p class="teacher-name"><a href="<?php echo bp_core_get_user_domain($teacher['ID']); ?>"> <?php echo bp_core_get_core_userdata($teacher['ID'])->display_name; ?></a>
                                            </p><!--/.teacher-name-->
                                        </div><!--/.teacher-info-->
                                        <?php
                                        if (xprofile_get_field_data('Certificate', $teacher['ID'])): ?>
                                            <p class="teacher-hour"><?php echo xprofile_get_field_data('Certificate', $teacher['ID']); ?> Certified</p><!--/.teacher-hour-->
                                        <?php endif; ?>
                                    </div><!--/.single-teacher-->
                                    <?php $counter++; ?>
                            <?php endforeach; ?>
                        </div><!--/.teachers-block-->
                    </div><!--/.container-->
                </div><!--/.teacher-training-block-->
            </div>
        <?php endif;?>
    </div>

<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>





<?php get_footer(); ?>



