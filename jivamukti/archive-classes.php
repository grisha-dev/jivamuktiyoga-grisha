<?php
get_header();
if (have_posts()) : ?>
    <section class="jv-classes upcoming-teacher-trainings text-center">
        <div class="container-fluid">
            <div class="row">
                <h1 class="block-title template-title brown">Class Descriptions</h1>
                <?php $counter = 0; ?>
                <?php while (have_posts()) : the_post();
                    include(locate_template('template-parts/nyc-classes.php',false,false));
                    $counter++;
                endwhile; ?>

            </div>
        </div>
    </section>
<?php endif;
get_footer();
?>
