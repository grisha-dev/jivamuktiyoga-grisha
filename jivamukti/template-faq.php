<?php
/**
 * Template Name: FAQ
 */
get_header();
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class('template-faq'); ?> >
        <div class="container jv-container">
        <hgroup class="template-title-group">
            <h1 class="block-title template-title brown"><?php the_title(); ?></h1>
        </hgroup>
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
        <?php get_template_part('template-parts/accordion','content'); ?>
        </div>
    </div>
    <?php endwhile; ?>
<?php else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?> </p>
<?php endif; ?>
<?php get_footer(); ?>
