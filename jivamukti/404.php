<?php
/**
 * The template for displaying error 404 page.
 *
 */
get_header(); ?>
    <div class="content inner-page clearfix">
        <div class="container search-inner">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="error404-message">
                        <hi class="block-title brown">Oops...</hi>
                        <p class="entry-content">It seems we can't find what you're looking for.<br>Please use the search below to type a keyword related <br>to what you are looking for.</p>
                    </div>
                    <div class="error404-search">
                        <?php get_search_form(); ?>

                    </div>

                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>