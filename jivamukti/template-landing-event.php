<?php

get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div id="post-<?php the_ID(); ?>" <?php post_class('template-landing-event'); ?> >

    <hgroup class="template-title-group">
        <h1 class="block-title template-title brown"><?php the_title(); ?></h1>
    </hgroup>
    <?php
    ?>
    <div class="container jv-container">
        <div class="filter-block">
            <div class="years-filter">
                <a href="<?php the_permalink(); ?>" class="upcoming-events active">Upcoming</a>

                    <span class="past-events-title">Past Events:</span>
                    <?php $flag = true;
                    $flag1 = true;
                    $flag2 = true;
                    $events = get_posts(array('post_type' => 'events', 'post_status' => 'publish', 'posts_per_page' => -1)) ?>
                    <?php foreach ($events as $post) {
                        setup_postdata($post);
                        if (date('Y', strtotime(get_field('begin'))) == date("Y") && $flag) { ?>
                            <a href="#"><?php echo date("Y"); ?></a>
                            <?php $flag = false;
                        } else if (date('Y', strtotime(get_field('begin'))) == date("Y", strtotime("-1 year")) && $flag1) { ?>
                            <a href="#"><?php echo date("Y", strtotime("-1 year")); ?></a>
                            <?php $flag1 = false;
                        } else if (date('Y', strtotime(get_field('begin'))) == date("Y", strtotime("-2 year")) && $flag2) { ?>
                            <a href="#"><?php echo date("Y", strtotime("-2 year")); ?></a>
                            <?php $flag2 = false;
                        }
                    }
                    wp_reset_postdata();
                    ?>

            </div>
<!--                <li><a href="#">--><?php //echo date("Y",strtotime("-1 year"));?><!--</a></li>-->
<!--                <li><a href="#">--><?php //echo date("Y",strtotime("-2 year"));?><!--</a></li>-->

            <form class="form-inline" id="event-filter-form" method="get" action="">
                <input type="hidden" name="event-year" value="<?php echo htmlspecialchars($_GET['event-year']);?>">

                <div class="form-group">
                    <label class="sr-only" for="event-location">Location</label>
                    <input type="text" class="form-control" id="event-location" name="event-location" value="<?php echo ($_GET['event-location']) ? $_GET['event-location'] : '';?>" placeholder="Location">
                </div>

                <div class="form-group">
                    <select class="form-control" name="event-type">

                            <option value="default" selected >Event type</option>

                        <?php $terms = get_terms('event_categories');
                        foreach ($terms as $term) {?>
                            <option><?php  echo $term->name; ?></option>
                        <?php }?>
                    </select>
                </div>
                <button type="submit" class="jv-btn-green">Apply</button>
            </form>
        </div>
    </div>
    <div class="events-block">
<!--        --><?php //var_dump($_GET); ?>
        <div class="container jv-container text-center">
            <div class="event-container">
                <?php
                // query
                $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
//                    var_dump($paged);
                $args = array(
                    'post_type'			=> 'events',
                    'posts_per_page'	=> 9,
                    'post_status'       => 'publish',
                    'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key' => 'begin',
                            'value'   => date('Y-m-d H:i:s'),
                            'type'    => 'DATETIME',
                            'compare' => '>',
                        ),
                    ),
                    'meta_key'			=> 'begin',
                    'orderby'			=> 'meta_value',
                    'order'				=> 'DESC',
                    'paged'             =>  $paged,
                );
                if ($_GET['event-year'] && !$_GET['event-location']) {
                    $year_range_start = $_GET['event-year'] . '-01-01 00:00:00';
                    $year_range_end = $_GET['event-year'] . '-12-31 23:59:59';
                    $args['meta_query'] = array(
                        array(
                            'key' => 'begin',
                            'value'   => array( $year_range_start, $year_range_end ),
                            'type'    => 'DATETIME',
                            'compare' => 'BETWEEN',
                        ),
                    );
                }
                elseif (!$_GET['event-year'] && $_GET['event-location']) {
                    array_push($args['meta_query'], array(
                        'key' => 'location',
                        'value'   => $_GET['event-location'],
                        'type'    => 'CHAR',
                        'compare' => 'LIKE',
                    ) );


                }
                elseif ($_GET['event-year'] && $_GET['event-location']) {
                    $year_range_start = $_GET['event-year'] . '-01-01 00:00:00';
                    $year_range_end = $_GET['event-year'] . '-12-31 23:59:59';
                    $args['meta_query'] = array(
                        'relation' => 'AND',
                        array(
                            'key' => 'begin',
                            'value'   => array( $year_range_start, $year_range_end ),
                            'type'    => 'DATETIME',
                            'compare' => 'BETWEEN',
                        ),
                        array(
                            'key' => 'location',
                            'value'   => $_GET['event-location'],
                            'type'    => 'CHAR',
                            'compare' => 'LIKE',
                        ),
                    );
                }
                if ($_GET['event-type'] && $_GET['event-type'] != 'default') {
                    $args['tax_query'] = array(
                        array(
                            'taxonomy' => 'event_categories',
                            'field'    => 'slug',
                            'terms'    => $_GET['event-type'],
                        ),
                    );
                }
//                var_dump($args);
                $the_query = new WP_Query($args);



                ?>
                <?php if( $the_query->have_posts() ): ?>
                    <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="event-item">

                                <div class="item-top-block">
                                    <?php if($image = get_field('image')):
                                        $img_medium = $image['sizes'][ 'medium' ];

                                        ?>
                                    <div class="event-img" style="background-image: url(<?php echo $img_medium; ?>)"></div>
                                    <?php else: ?>
                                        <div class="noimg-block"><img src="<?php echo get_field('opt_logo', 'options') ?>" alt="" ></div>
                                    <?php endif; ?>
                                    <div class="event-info">
                                        <p class="event-i-item event-name">
                                            <?php the_title();?>
                                        </p>
                                        <p class="event-i-item">
                                            <?php if(get_field('begin')): ?>
                                            <span><b>Begin:</b> <?php the_field('begin'); ?></span>
                                            <?php endif;?>
                                            <?php if(get_field('end')): ?>
                                            <span><b>End:</b> <?php the_field('end'); ?></span>
                                            <?php endif;?>
                                        </p>
                                        <?php if(get_field('location')): ?>
                                        <p class="event-i-item">
                                            <span><b>Location:</b> <?php echo get_field('location')['address']; ?></span>
                                        </p>
                                        <?php endif;?>
                                        <?php if(get_field('price')): ?>
                                        <p class="event-i-item">
                                            <span><b>Price:</b> <?php the_field('price'); ?></span>
                                        </p>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <div class="button-block">
                                    <a href="<?php the_field('registration_url'); ?>" class="jv-btn-green">Register</a>
                                </div>
                            <a href="<?php the_permalink();?>"><span></span></a>
                        </div>


                    <?php endwhile;?>
               
            </div>
                    <div class="pagination-links">
                    <?php
                    $big = 999999999; // need an unlikely integer

                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $the_query->max_num_pages
                    ) );

                    ?>
        </div>
                  <?php wp_reset_postdata();?>
            <?php endif; ?>

        </div><!--/.container-->
    </div><!--/.events-block-->
</div>
<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>





<?php get_footer(); ?>
